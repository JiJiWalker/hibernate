package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


/**
 * Created by Jakub on 01.08.2017.
 */
public class ReadStudentDemo {
    public static void main(String[] args) {

//        create session factory
        SessionFactory factory = new Configuration()
                                .configure("hibernate.cfg.xml") // standardowo może być bez nazwy xml.
                                .addAnnotatedClass(Student.class)
                                .buildSessionFactory();

//        create session
        Session session = factory.getCurrentSession();

        try {
//            create a student object
            System.out.println("Creating new student object...");
            Student xStudent1 = new Student("Bunny", "Munore", "bm@bm.bm");

//            start a transaction
            session.beginTransaction();

//            save the student object
            System.out.println("Saving the student...");
            System.out.println(xStudent1);
            session.save(xStudent1);


//            commit transaction
            session.getTransaction().commit();

//            MY NEW CODE

//            find out the students id" primary key
            System.out.println("Saved student. Generated id: "+xStudent1.getId());

//            now get a new session and start transaction
            session = factory.getCurrentSession();
            session.beginTransaction();

//            retrieve student basen od the id: primary key
            System.out.println("\nGetting student with id: "+xStudent1.getId());

            Student myStudent = session.get(Student.class, xStudent1.getId());

            System.out.println("Get complete: "+"\n" + myStudent);
//            commit the transaction
            session.getTransaction().commit();

            System.out.println("Done.");

        } finally {
            factory.close();
        }
    }
}
