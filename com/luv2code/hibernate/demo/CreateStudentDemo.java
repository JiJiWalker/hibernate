package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


/**
 * Created by Jakub on 01.08.2017.
 */
public class CreateStudentDemo {
    public static void main(String[] args) {

//        create session factory
        SessionFactory factory = new Configuration()
                                .configure("hibernate.cfg.xml") // standardowo może być bez nazwy xml.
                                .addAnnotatedClass(Student.class)
                                .buildSessionFactory();

//        create session
        Session session = factory.getCurrentSession();

        try {
//            create a student object
            System.out.println("Creating new student object...");
            Student xStudent1 = new Student("Paul", "Walker", "ff6@furious.com");

//            start a transaction
            session.beginTransaction();

//            save the student object
            System.out.println("Saving the student...");
            session.save(xStudent1);


//            commit transaction
            session.getTransaction().commit();
            System.out.println("Done.");

        } finally {
            factory.close();
        }
    }
}
