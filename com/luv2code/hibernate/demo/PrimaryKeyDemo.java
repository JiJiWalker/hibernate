package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Jakub on 01.08.2017.
 */
public class PrimaryKeyDemo {
    public static void main(String[] args) {

        //        create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml") // standardowo może być bez nazwy xml.
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

//        create session
        Session session = factory.getCurrentSession();

        try {
//            create 3 student objects
            System.out.println("Creating 3 students object...");
            Student xStudent1 = new Student("Paul", "Walker", "ff6@furious.com");
            Student xStudent2 = new Student("John", "Paul-Johnes", "jpj@noquarter.com");
            Student xStudent3 = new Student("James", "Howllet", "wolverine@fcku.com");

//            start a transaction
            session.beginTransaction();

//            save the student object
            System.out.println("Saving the student...");
            session.save(xStudent1);
            session.save(xStudent2);
            session.save(xStudent3);


//            commit transaction
            session.getTransaction().commit();
            System.out.println("Done.");

        } finally {
            factory.close();
        }

    }
}
