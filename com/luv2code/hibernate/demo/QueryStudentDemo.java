package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


import javax.persistence.Query;
import java.util.List;


/**
 * Created by Jakub on 01.08.2017.
 */
public class QueryStudentDemo {
    public static void main(String[] args) {

//        create session factory
        SessionFactory factory = new Configuration()
                                .configure("hibernate.cfg.xml") // standardowo może być bez nazwy xml.
                                .addAnnotatedClass(Student.class)
                                .buildSessionFactory();

//        create session
        Session session = factory.getCurrentSession();

        try {
//            start a transaction
            session.beginTransaction();

//            query student
            String fromStudent = "from Student";
            Query query = session.createQuery(fromStudent);
            List<Student> theStudents = query.getResultList();

//            display the students
            for (Student tempStudent : theStudents) {
                System.out.println(tempStudent);

            }

//            query students: lastName='Doe'



//            commit transaction
            session.getTransaction().commit();
            System.out.println("Done.");

        } finally {
            factory.close();
        }
    }
}
